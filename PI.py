import numpy as np
import pandas as pd


class PI:
    def __init__(self, actions, env, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.env = env
        self.env.canvas.delete(self.env.agent)
        self.lr = learning_rate
        self.theta = 0.01
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.v_table = pd.DataFrame(columns=['v'], dtype=np.float64)
        self.policy = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.init_tables()
        self.display_name="PI"
    
    def init_tables(self):
        # Iterate through columns
        for x in range(self.env.MAZE_W):
            # Iterate through rows
            for y in range(self.env.MAZE_H):
                state = self.get_state(x, y)
                # # Don't calculate for walls or pits
                self.v_table = self.v_table.append(
                    pd.Series(
                        [0],
                        index=self.v_table.columns,
                        name=str(state),
                    )
                )
                self.policy = self.policy.append(
                    pd.Series(
                        [float(1)/len(self.actions)]*len(self.actions),
                        index=self.policy.columns,
                        name=str(state),
                    )
                )
        self.policy_improvement()
        self.env.reset()

    def policy_evaluation(self):
        while True:
            delta = 0
            # Iterate through columns
            for x in range(self.env.MAZE_W):
                # Iterate through rows
                for y in range(self.env.MAZE_H):
                    state = self.get_state(x, y)
                    # Don't calculate v for walls or pits
                    if state not in [self.env.canvas.coords(w) for w in self.env.wallblocks] and state not in [self.env.canvas.coords(w) for w in self.env.pitblocks]:
                        v = self.v_table.loc[str(state)][0]
                        new_v = 0
                        # Iterate through all actions
                        for action, action_prob in self.policy.loc[str(state)].items():
                            self.env.add_agent(x, y)
                            state_, reward, done = self.env.step(action)
                            self.env.canvas.delete(self.env.agent)
                            # Note that probability of moving from one state to another given an action a is 1
                            new_v += action_prob*(reward + self.gamma*self.v_table.loc[str(state_)][0])
                        self.v_table.loc[str(state)][0] = new_v
                        delta = max(delta, abs(v - new_v))
            if delta < self.theta:
                break

    def policy_improvement(self):
        while True:
            self.policy_evaluation()
            policy_stable = True
            # Iterate through columns
            for x in range(self.env.MAZE_W):
                # Iterate through rows
                for y in range(self.env.MAZE_H):
                    state = self.get_state(x, y)
                    old_action = self.policy.loc[str(state)].idxmax()
                    # Calculate v for all actions
                    v = []
                    for action in self.actions:
                        self.env.add_agent(x, y)
                        state_, reward, done = self.env.step(action)
                        self.env.canvas.delete(self.env.agent)
                        v.append(reward + self.gamma*self.v_table.loc[str(state_)][0])
                    # Get best action
                    new_action = np.argmax(v)
                    # Update policy with the single best action (first action if there's a tie)
                    self.policy.loc[str(state)] = [1 if i == new_action else 0 for i in self.actions]
                    if new_action != old_action:
                        policy_stable = False
            if policy_stable:
                break

    def get_state(self, x, y):
        return [x*self.env.UNIT + float(5), y*self.env.UNIT + float(5), (x+1)*self.env.UNIT - float(5), (y+1)*self.env.UNIT - float(5)]


    def choose_action(self, state):
        return self.policy.loc[str(state)].idxmax()

    def learn(self, s, a, r, s_):
        a_ = self.choose_action(str(s_))
        return s_, a_
